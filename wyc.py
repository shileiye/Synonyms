#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
# Copyright (c) 2022 <> All Rights Reserved
#
#
# File: wyc.py
# Author: shileiye
# Date: 2022-11-03 16:59:34
#
# =========================================================================

"""

"""
from __future__ import print_function
from __future__ import division

__copyright__ = "Copyright (c) (2017-2022) Chatopera Inc. All Rights Reserved"
__author__ = "shileiye"
__date__ = "2022-11-03 16:59:34"

import os
import re
import sys

curdir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(curdir)

if sys.version_info[0] < 3:
    reload(sys)
    sys.setdefaultencoding("utf-8")
    # raise "Must be using Python 3"
    # 

import synonyms  # https://github.com/huyingxi/Synonyms
import numpy
import unittest
from selectolax.parser import HTMLParser
compare_ = lambda x, y, z: "%s vs %s: %f" % (x, y, synonyms.compare(x, y, seg=z)) + "\n" + "*" * 30 + "\n"


# run testcase: python wyc.py Test.testExample
class Test(unittest.TestCase):
    '''

    '''

    def test_wordseg(self):
        print("伪原创测试")
        newtext = wordtext = '<p style="text-indent: 2em; text-align: left; margin-top: 5px;">▶<a href="https://news.woyaobid.com/tags-1922.html" title="亏欠效应" target="_blank">亏欠效应</a>◀</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">我们依然在“<a href="https://news.woyaobid.com/tags-1921.html" title="谈判" target="_blank">谈判</a>的双管齐下”这个系列推文中，它的思维方式来自哈佛商学院的“三维谈判系统”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">谈判有三个维度。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">在第一个维度的“讨价还价”中，我们需要谨慎地保护我们自己的信息；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">而在第二个维度的“协议构思”中，我们需要尽可能和对方一起分享信息，创造更多的价值；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">这是一个两难的处境。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">这个系列就是要帮我们解决这个问题，该如何在第一个和第二个维度中“双管齐下”？</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">前两篇篇我们聊了两个方法：“聆听”和“以终为始”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">今天我们聊第三个方法：“亏欠效应”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">简单地理解，“亏欠效应”是一种心理现象，它指的是，</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">我们对于别人所给我们的，抱有“亏欠”，总是倾向于“归还”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">我们中国人最常用来描述这个现象的词叫做“人情”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">中国社会甚至被人叫做最典型的“人情社会”。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">但可别真的认为这是咱们国家的特产，亚洲的其它地区，“人情现象”也很普遍。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">据说在巴基斯坦和印度的某些地域文化中，当参加婚礼的人离开的时候，举办方会给客人发一种类似“随手礼”的糖；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">但有意思的是，他们会在发“糖”的时候，说的很“直白”，</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">这五盒是你们上次结婚送我们的，这五盒是我们这次的。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">咱们中国人在结婚礼金上的习惯很类似，虽然大多数时候我们不会当面拆红包点礼金数量，但习惯上如果对方上次给的礼金是500元，这次我们的回礼一定会大于500。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">不知道你有没有发现，我们在对待“人情”的“亏欠”和“归还”上，似乎总是故意让“归还”的数量大于“亏欠”，这样就会让对方继续“亏欠”我们；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">所谓“人情社会”，其本质就是“人情”反复“亏欠”和“归还”之后，所产生的错综复杂的一张网。</p><p style="text-align: left; text-indent: 2em; margin-top: 5px;">&nbsp;</p><p style="text-align:center"><img src="//www.zfcgw.com/html/upload/2022/10-17/11-25-480621-1386015899.png" title="14.png" alt="14.png"/></p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">02</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">▶实验基础◀</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">“亏欠”会产生强大的心理作用，从而影响人们的行为决策。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">有一个非常著名的心理学实验是这样设计的，请受试者参与某一个活动，工作人员会悄悄扮演一个受试者参与其中；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">房间的温度故意设定得比较热，不经意间，工作人员会和受试者打个招呼出去买汽水；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">回来时，工作人员会特意多买一瓶给受试者，大部分情况下，受试者都会欣然接受并表示感谢；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">随后，工作人员会向受试者提出请求，请他们帮忙完成自己卖彩票的任务，也就是请他们买几张彩票。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">实验从两个维度做出统计，一个是“是否向受试者提供免费汽水”，另一个是“受试者是否喜欢工作人员”；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">结果很明显，被提供免费汽水的受试者更容易答应买彩票的请求，因为他们心中被“免费汽水”的“亏欠”所影响；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">更值得关注的是，哪怕是那些经过调查表明很不喜欢工作人员的受试者，也会变得更容易被“免费汽水”影响而答应买彩票。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">所以，“亏欠效应”所发挥的心理威力，甚至可以抗衡一个人对另一个人的好恶感。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">这个结论有着巨大的启示，即使我们的谈判对手不喜欢我们，依然无法抵抗“亏欠效应”的影响。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">03</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">▶商业应用◀</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">在商业活动中，“亏欠效应”的应用多不胜数。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">你能看到的“免费试用品”都属于这类策略。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">商家用各种免费品尝、免费试用的手段，来让消费者产生“亏欠”心理，从而会进一步采取“购买手段”，来“归还”补偿。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">1957年，在美国印第安纳州的一家超市，促销人员把大块的起司直接拿到柜台外面，让路过的顾客自己用刀来切片免费品尝；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">这个尝试让这家超市在几个小时内，卖掉了整整1000磅的起司。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">哪怕在服务行业，服务生也非常善于用这个办法来提高客人的消费；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">据统计，如果服务生在客人买单的时候，随账单附送一颗糖，他们拿到的小费平均提高3.3%；</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">如果他们可以每次附送两颗糖，小费平均会提高14%。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">然而，这还不是“亏欠效应”所能发挥最大威力的领域。</p><p style="text-indent: 2em; text-align: left; margin-top: 5px;">在“谈判思维”中，“亏欠效应”有着更加神奇的运用。</p>'
        # 清除HTML标记
        wordtext = HTMLParser(wordtext).text()
        print("原文：", wordtext)
        # 处理锁词，支持正则
        reglist = [r'第.*?条', r'《.*?》', r'\(.*?\)', r'“.*?”', r'【.*?】', r'\[.*?\]', r'\{.*?\}', r'[a-zA-Z0-9]*?', r'（.*?）', r'".*?"', r'\'.*?\'', r'中华人民共和国']
        for reg in reglist:
            wordtext = re.sub(reg, '', wordtext)
        print("去除锁词文：", wordtext)
        wordseg = synonyms.seg(wordtext)
        print("分词结果：", wordseg)
        # 分词去重
        newwordseg = [[], []]
        i = 0
        for x in wordseg[0]:
            if x not in newwordseg[0]:
                newwordseg[0].append(x)
                newwordseg[1].append(wordseg[1][i])
            i += 1
        print("分词去重结果：", newwordseg)
        cxs = ['a', 'ad', 'c', 'd', 'dg', 'e', 'f', 'i', 'o', 't', 'u', 'ud', 'ug', 'uj', 'ul', 'uv', 'uz', 'v', 'vg', 'p', 'm', 'n']
        i = 0
        kthc = []
        ythc = []
        for cx in newwordseg[1]:
            # 通过词性筛选需替换的词语
            if cx in cxs:
                # print("1.疑似可替换：", wordseg[0][i])
                # 获取当前词的近义词列表
                jyc = synonyms.nearby(newwordseg[0][i], 2)
                # print("近义词列表：", jyc)
                # 近义词列表词数大于1并且相似度高于0.8则选其第2个词作为替换词（第一个是词本身）
                if len(jyc[1]) > 1 and 1 > jyc[1][1] >= 0.8:
                    # print("找到近义词：", jyc)
                    # print("确定替换：", wordseg[0][i], "为：", jyc[0][1])
                    # 文本替换处理,每个词最多替换7次
                    newtext = newtext.replace(newwordseg[0][i], jyc[0][1], 7)
                    kthc.append(newwordseg[0][i])
                    ythc.append(jyc[0][1])
                    # print("可替换词：", kthc)
                    # print("已替换词：", ythc)
            i += 1
        # 处理自定义替换，支持正则
        zdycs=[[r'亏(.)效应', r'缺\1效应'], [r'我(.)', r'我们\1']]
        for zdyc in zdycs:
            newtext = re.sub(zdyc[0], zdyc[1], newtext)
        print("新文：", newtext)
        print("可替换词：", kthc)
        print("已替换词：", ythc)

def test():
    unittest.main()

if __name__ == '__main__':
    test()
